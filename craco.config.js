const CracoAlias = require("craco-alias");

module.exports = {
  plugins: [
    {
      plugin: CracoAlias,
      options: {
        source: "options",
        baseUrl: "./",
        aliases: {
          "@Config":     "./src/config",
          "@Images":     "./src/assets/images",
          "@Fonts":      "./src/assets/fonts",
          "@Style":      "./src/assets/scss",
          "@Components": "./src/components",
          "@Helpers":    "./src/helpers",

          // Modules
          "@Application":     "./src/modules/Application",
          "@Admin":           "./src/modules/Admin",
          "@Auth":            "./src/modules/Auth",
          "@Company":         "./src/modules/Company",
          "@Dashboard":       "./src/modules/Dashboard",
          "@Patients":        "./src/modules/Patients",
          "@UsersManagement": "./src/modules/UsersManagement",
          "@Scheduling":      "./src/modules/Scheduling",
          "@MedicalRecords":  "./src/modules/MedicalRecords",

        }
      }
    }
  ]
};