import axios  from 'axios';
import config from '@Config/service';

const Service = axios.create({
  baseURL: `${config.domain}${config.port ? ':' + config.port : ''}${config.namespace}`,
  timeout: (1000 * 60),
  headers: {
    'X-User-Email':  null,
    'X-User-Token':  null,
    'Authorization': null,
    'Content-Type': 'application/json'
  }
});

export default Service;
