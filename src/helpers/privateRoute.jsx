import React from 'react';
import { Redirect, Route } from "react-router-dom";

import { isUserAuthenticated } from '@Helpers/authUtils';
import Authorization from '@Application/helpers/Authorization';

const PrivateRoute = ({ component: Component, roles, ...rest }) => (
  <Route {...rest} render={props => {
    const isAuthTokenValid = isUserAuthenticated();
    if (!isAuthTokenValid) {
      // not logged in so redirect to login page with the return url
      return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
    }

    // Vererifica se o usuário possui permissão para acesso a rota
    if(
      roles && 
      roles.length && 
      !Authorization.allowed(roles)
    ){
      return <Redirect to={{ pathname: '/' }} />
    }

    // authorised so return component
    return <Component {...props} />
  }} />
)

export default PrivateRoute;