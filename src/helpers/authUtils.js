import jwtDecode from 'jwt-decode';

/**
 * Checks if user is authenticated
 */
const isUserAuthenticated = () => {
    const user = getLoggedInUser();
    if (!user) {
        return false;
    }
    const decoded = jwtDecode(user.token);
    const currentTime = Date.now() / 1000;
    if (decoded.exp < currentTime) {
        console.warn('access token expired');
        return false;
    }

    return true;
}

/**
 * Returns the logged in user
 */
const getLoggedInUser = () => {
    let user = localStorage.getItem('user');

    if(user){
        user = JSON.parse(user);
    }
    
    return user;
}

export { isUserAuthenticated, getLoggedInUser };