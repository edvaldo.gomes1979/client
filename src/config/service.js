export default {
    domain:    process.env.REACT_APP_SERVICE_HOST,
    port:      process.env.REACT_APP_SERVICE_PORT,
    namespace: process.env.REACT_APP_SERVICE_NAMESPACE
};
