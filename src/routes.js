import Auth      from '@Auth/routes';
import Dashboard from '@Dashboard/routes';
import UsersManagement from '@UsersManagement/routes';
import Patients        from '@Patients/routes';
import Scheduling      from '@Scheduling/routes';
import MedicalRecords  from '@MedicalRecords/routes';
import Admin           from '@Admin/routes';

let routes = [];

routes = routes.concat(Auth);
routes = routes.concat(Dashboard); 
routes = routes.concat(UsersManagement);
routes = routes.concat(Patients);
routes = routes.concat(Scheduling);
routes = routes.concat(MedicalRecords);
routes = routes.concat(Admin);

export { routes };