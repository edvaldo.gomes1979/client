import React from 'react';

import PrivateRoute from '@Helpers/privateRoute';

const MedicalRecords = React.lazy(()  => import('./pages'));

const routes = [
    { path: '/medical-records', name: 'MedicalRecords', component: MedicalRecords, route: PrivateRoute, roles: ['medical_records']},
];

export default routes;