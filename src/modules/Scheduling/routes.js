import React from 'react';

import PrivateRoute from '@Helpers/privateRoute';

const Scheduling = React.lazy(()  => import('./pages'));

const routes = [
    { path: '/scheduling', name: 'Scheduling', component: Scheduling, route: PrivateRoute, roles: ['scheduling']},
];

export default routes;