import React from 'react';

import PrivateRoute from '@Helpers/privateRoute';

const Patients = React.lazy(()  => import('./pages/Patients'));

const routes = [
    { path: '/patients', name: 'Patients', component: Patients, route: PrivateRoute, roles: ['patients']},
];

export default routes;