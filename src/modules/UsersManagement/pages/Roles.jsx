import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'reactstrap';

import Loader from '@Components/Loader';

class UsersManagement extends Component {

    render() {
        return (
            <React.Fragment>
                <div className="users-management">
                    {this.props.loading && <Loader />}

                    <Row>
                        <Col>
                            <div className="page-title-box">
                                <Row>
                                    <Col lg={7}>

                                    </Col>
                                    <Col lg={5} className="mt-lg-3 mt-md-0">
                                        
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                    </Row>
                </div>
            </React.Fragment>
        )
    }
}


export default connect()(UsersManagement);