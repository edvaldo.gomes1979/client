import React from 'react';
import { Redirect } from 'react-router-dom';

import PrivateRoute from '@Helpers/privateRoute';

const Users       = React.lazy(()  => import('./pages/Users'));
const Roles       = React.lazy(()  => import('./pages/Roles'));
const Permissions = React.lazy(()  => import('./pages/Permissions'));

const routes = [
    { path: '/users_management/users',       name: 'Users',      component: Users,      route: PrivateRoute, roles: ['users_management.users']},
    { path: '/users_management/roles',       name: 'Roles',      component: Roles,      route: PrivateRoute, roles: ['users_management.roles']},
    { path: '/users_management/permissions', name: 'Permission', component: Permissions, route: PrivateRoute, roles: ['users_management.permissions']},

    {
        path: "/users_management",
        exact: true,
        component: () => <Redirect to="/users_management/users" />,
        route: PrivateRoute
    },
];

export default routes;