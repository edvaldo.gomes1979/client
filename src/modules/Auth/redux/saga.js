// @flow
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';

import {
    LOGIN_USER,
    LOGOUT_USER
} from '@Auth/constants/actionTypes';


import {
    loginUserSuccess,
    loginUserFailed
} from './actions';

import Service from '@Helpers/service';

/**
 * Sets the session
 * @param {*} user 
 */
const setSession = (user) => {
    if (user){
        Service.defaults.headers['X-User-Email']  = user.email;
        Service.defaults.headers['X-User-Token']  = user.access_token;
        Service.defaults.headers['Authorization'] = user.token;

        localStorage.setItem('user', JSON.stringify(user));
        return;
    }

    Service.defaults.headers['X-User-Email']  = null;
    Service.defaults.headers['X-User-Token']  = null;
    Service.defaults.headers['Authorization'] = null;
    localStorage.removeItem('user');
};

/**
 * Login the user
 * @param {*} payload - username and password 
 */
function* login({ payload: { username, password } }) {
    try {
        const { data } = yield call(() => Service.post('v1/users/sign_in', {email: username, password}));

        setSession(data);
        yield put(loginUserSuccess(data));
    } catch (error) {
        let message = 'Usuário ou senha inválida!';
        yield put(loginUserFailed(message));
        setSession(null);
    }
}

/**
 * Logout the user
 * @param {*} param0 
 */
function* logout({ payload: { history } }) {
    try {
        setSession(null);
        yield call(() => {
            history.push("/login");
        });
    } catch (error) { }
}

export function* watchLoginUser():any {
    yield takeEvery(LOGIN_USER, login);
}

export function* watchLogoutUser():any {
    yield takeEvery(LOGOUT_USER, logout);
}

function* authSaga():any {
    yield all([
        fork(watchLoginUser),
        fork(watchLogoutUser)
    ]);
}

export default authSaga;