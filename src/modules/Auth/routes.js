import React from 'react';
import { Route } from 'react-router-dom';

const Login  = React.lazy(()  => import('./pages/Login'));
const Logout = React.lazy(()  => import('./pages/Logout'));

const ForgetPassword = React.lazy(() => import('./pages/ForgetPassword'));
const Register       = React.lazy(() => import('./pages/Register'));
const ConfirmAccount = React.lazy(() => import('./pages/Confirm'));

const routes = [
  { path: '/login',           name: 'Login',            component: Login,          route: Route },
  { path: '/logout',          name: 'Logout',           component: Logout,         route: Route },
  { path: '/forget-password', name: 'Forget Password',  component: ForgetPassword, route: Route },
  { path: '/register',        name: 'Register',         component: Register,       route: Route },
  { path: '/confirm',         name: 'Confirm',          component: ConfirmAccount, route: Route }  
];

export default routes;