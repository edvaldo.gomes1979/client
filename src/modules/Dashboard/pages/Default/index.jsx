import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'reactstrap';

import { getLoggedInUser } from '@Helpers/authUtils';
import Loader from '@Components/Loader';
import Modules from '@Dashboard/components/modules/Modules';

class DefaultDashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: getLoggedInUser()
        };
    }

    render() {
        return (
            <React.Fragment>
                <div className="dashboard">
                    { /* preloader */}
                    {this.props.loading && <Loader />}

                    <Row>
                        <Col>
                            <div className="page-title-box">
                                <Row>
                                    <Col lg={7}>

                                    </Col>
                                    <Col lg={5} className="mt-lg-3 mt-md-0">
                                        
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                    </Row>

                    <Modules />
                </div>
            </React.Fragment>
        )
    }
}


export default connect()(DefaultDashboard);