import React from 'react';
import { Redirect } from 'react-router-dom';
import PrivateRoute from '@Helpers/privateRoute';

const Dashboard  = React.lazy(()  => import('./pages/Default'));

const routes = [
    { path: '/dashboard', name: 'Dashboard', component: Dashboard, route: PrivateRoute},
    {
        path: "/",
        exact: true,
        component: () => <Redirect to="/dashboard" />,
        route: PrivateRoute
    },
];

export default routes;