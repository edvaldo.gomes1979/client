import React from 'react';
import { Link } from 'react-router-dom';

const Module = ({ module }) => {
    return (
        <Link className="col-xl-3 col-md-6" to={module.path}>
            <div className="widget-bg-color-icon card-box">
                <div className={'avatar-md rounded-circle float-left text-center ' + module.color}>
                    <i className={'font-24 avatar-title text-white ' + module.icon}></i>
                </div>
                <div className="text-left ml-5">
                    <h3 className="text-dark">{module.name}</h3>
                </div>
                <div className="clearfix"></div>
            </div>
        </Link>
    );
};

export default Module;