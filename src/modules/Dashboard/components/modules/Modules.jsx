import React from 'react';
import _ from 'lodash';

import UserHelper from '@Application/helpers/UserHelper';
import Module from './Module';

const Modules = () => {
    const rows = _.chunk(UserHelper.modules(), 4);
    return (
        <>
            {rows.map((row, rowKey) =>
                <div className="row" key={rowKey}>
                    {row.map((module, moduleKey) => 
                        <Module key={moduleKey} module={module} />
                    )}
                </div>
            )}
        </>
    );
};

export default Modules;