const _ = require('lodash');

const storage = JSON.parse(localStorage.getItem('user'));

class UserHelper{
    
    /**
     * Obtem um valor da sessão do usuário
     *
     * @param { String }
     * @param { Any } default value
     * @param { Any }
     */
    static get(field, dft = null){
        return _.get(storage, field, dft);
    }

    /**
     * Obtem os modulos que o usuário possui acesso
     *
     * @return { Array }
     *
     */
    static modules(){
        if(this._modules){
            return this._modules;
        }

        this._modules = this.get('modules', []);
        return this._modules;
    }

    /**
     * Obtem os papeis do usuário
     *
     * @return { Array }
     *
     */
    static roles(){
        return this.get('roles', []);
    }

    /**
     * Obtem todas as permissões do usuário
     *
     * @return { Array }
     *
     */
    static permissions(){
        if(this._permissions){
            return this._permissions;
        }

        let roles = this.roles();


        let permissions = [];
        roles.forEach((role) => {
            permissions = permissions.concat(role.permissions.map(i => i.name));
        });

        this._permissions = _.uniq(permissions);
        return this._permissions;
    }
}

export default UserHelper;