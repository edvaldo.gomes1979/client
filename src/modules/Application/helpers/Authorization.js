import UserHelper from './UserHelper';

class Authorization{

    static allowed(rules = []){
        if(!rules.length){
            return true;
        }

        let permissions = UserHelper.permissions();

        return rules.some((rule) => permissions.indexOf(rule) > -1);
    }

}

export default Authorization;