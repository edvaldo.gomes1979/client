import UserHelper from './UserHelper';
const _ = require('lodash');

const defaultModule = {
    name: 'Dashboard',
    color: 'md-light-blue-300-bg',
    icon:  'remixicon-dashboard-line',
    modules: []
};

class Module{
   
   /**
    * Obtem o menu com base no módulo atual
    *
    * @param  { Route }
    * @return { Object }
    *
    */
   static getMenuByCurrentPage(route){
       route = _.compact(route.path.split('/'))[0];

       let modules = UserHelper.modules();
       modules = modules.find((i) => i.slug === route);

       return modules ? modules : defaultModule;
   }

   /**
    * Veririca se o modulo é o módulo atual
    *
    * @param { String }
    * @return { Boolean }
    * 
    */
    static hasActive(module, route){
        console.log(route.path, ' ', module.path, ' = ', route.path.indexOf(module.path) > -1);
        return route.path.indexOf(module.path) > -1
    }
}

export default Module;