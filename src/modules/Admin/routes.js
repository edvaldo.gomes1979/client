import React from 'react';

import PrivateRoute from '@Helpers/privateRoute';

const Admin = React.lazy(()  => import('./pages'));

const routes = [
    { path: '/admin', name: 'Admin', component: Admin, route: PrivateRoute, roles: ['administrator']},
];

export default routes;