import React, { Component, Suspense } from "react";
import { Container } from 'reactstrap';
import { connect } from 'react-redux';

import profilePic from '../assets/images/users/default.png';
import { getLoggedInUser } from '@Helpers/authUtils';

// code splitting and lazy loading
// https://blog.logrocket.com/lazy-loading-components-in-react-16-6-6cea535c0b52
const Topbar = React.lazy(() => import("./Topbar"));
const Navbar = React.lazy(() => import("./Navbar"));
const RightSidebar = React.lazy(() => import("./RightSidebar"));

const loading = () => <div className="text-center"></div>;


const RightSidebarContent = () => {
    let currentUser = getLoggedInUser();

    return (
        <div className="user-box">
            <div className="user-img">
                <img src={profilePic} alt="user-img" title="Nik Patel"
                    className="rounded-circle img-fluid" />
                <a href="/" className="user-edit"><i className="mdi mdi-pencil"></i></a>
            </div>
            {currentUser &&
                <>
                    <h5><a href="/">{currentUser.name}</a></h5>
                    <p className="text-muted mb-0"><small>{currentUser.roles[0].display_name}</small></p>
                </>
            }
        </div>
    );
}

class AuthLayout extends Component {

    constructor(props) {
        super(props);

        this.toggleRightSidebar = this.toggleRightSidebar.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);
        this.state = {
            isMenuOpened: false
        }
    }

    signOut(e) {
        e.preventDefault();
        this.props.history.push("/login");
    }

    /**
     * toggle Menu
     */
    toggleMenu = (e) => {
        e.preventDefault();
        this.setState({isMenuOpened: !this.state.isMenuOpened});
    }

    /**
     * Toggle right side bar
     */
    toggleRightSidebar = () => {
        document.body.classList.toggle("right-bar-enabled");
    }

    render() {
        // get the child view which we would like to render
        const children = this.props.children || null;

        return (
            <div className="app">
                <header id="topnav">
                    <Suspense fallback={loading()}>
                        <Topbar rightSidebarToggle={this.toggleRightSidebar} menuToggle={this.toggleMenu} isMenuOpened={this.state.isMenuOpened} />
                        <Navbar isMenuOpened={this.state.isMenuOpened} {...this.props}/>
                    </Suspense>
                </header>

                <div className="wrapper">
                    <Container fluid>
                        <Suspense fallback={loading()}>
                            {children}
                        </Suspense>
                    </Container>
                </div>

                <RightSidebar title={"Settings"}>
                    <RightSidebarContent />
                </RightSidebar>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        user: state.Auth.user
    }
}
export default connect(mapStateToProps, null)(AuthLayout);
