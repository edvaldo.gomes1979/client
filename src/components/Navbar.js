import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Collapse } from 'reactstrap';

import Module from '@Application/helpers/Module';

const NavMenuContent = (props) => {
    const current = Module.getMenuByCurrentPage(props.match);
    const onMenuClickCallback = props.onMenuClickCallback;

    return <React.Fragment>
        <ul className="navigation-menu">
            <span 
                className={'current-module avatar-sm rounded-circle float-left text-center ' + current.color}
                title={current.name}
            >
                <i className={'avatar-title text-white ' + current.icon}></i>
            </span>
            
            {current.modules.map((item, itemKey) =>
                <li className={(Module.hasActive(item, props.match) ? 'has-submenu active' : '')} key={itemKey}>
                    <Link to={item.path} aria-expanded={item.modules.length > 0} onClick={onMenuClickCallback}>
                        <i className={item.icon}></i>
                        {item.name}

                        {item.modules.length > 0 &&
                            <div className="ml-1 arrow-down"></div>
                        }
                    </Link>
                    {item.modules.length > 0 &&
                        <ul className="submenu">
                            {item.modules.map((secondary, secondaryKey) => 
                                <li className={secondary.modules.length > 0 ? 'has-submenu' : ''} key={secondaryKey}>
                                    <Link to={secondary.path} onClick={onMenuClickCallback}>
                                        {secondary.modules.length > 0 &&
                                            <div className="arrow-down"></div>
                                        }
                                        {secondary.name}
                                    </Link>

                                    {secondary.modules.length > 0 &&
                                        <ul className="submenu">
                                            {secondary.modules.map((third, thirdKey) => 
                                                <li key={thirdKey}>
                                                    <Link to={third.path} className="side-nav-link-ref">{third.name}</Link>
                                                </li>
                                            )}
                                        </ul>
                                    }
                                </li>
                            )}
                        </ul>
                    }
                </li>
            )}
        </ul>
    </React.Fragment>
}


class Navbar extends Component {
    constructor(props) {
        super(props);

        this.initMenu = this.initMenu.bind(this);
    }

    /**
     * 
     */
    componentDidMount = () => {
        this.initMenu();
    }

    /**
     * Init the menu
     */
    initMenu = () => {
        var links = document.getElementsByClassName('side-nav-link-ref');
        var matchingMenuItem = null;
        for (var i = 0; i < links.length; i++) {
            if (this.props.location.pathname === links[i].pathname) {
                matchingMenuItem = links[i];
                break;
            }
        }

        if (matchingMenuItem) {
            matchingMenuItem.classList.add('active');
            var parent = matchingMenuItem.parentElement;

            /**
             * TODO: This is hard coded way of expading/activating parent menu dropdown and working till level 3. 
             * We should come up with non hard coded approach
             */
            if (parent) {
                parent.classList.add('active');
                const parent2 = parent.parentElement;
                if (parent2) {
                    parent2.classList.add('in');
                }
                const parent3 = parent2.parentElement;
                if (parent3) {
                    parent3.classList.add('active');
                    var childAnchor = parent3.querySelector('.has-dropdown');
                    if (childAnchor) childAnchor.classList.add('active');
                }

                const parent4 = parent3.parentElement;
                if (parent4)
                    parent4.classList.add('in');
                const parent5 = parent4.parentElement;
                if (parent5)
                    parent5.classList.add('active');
            }
        }
    }

    /**
     * On menu clicked event
     * @param {*} event 
     */
    onMenuClick(event) {
        // event.preventDefault();
        const nextEl = event.target.nextSibling;
        if (nextEl && nextEl.classList && !nextEl.classList.contains('open')) {
            const parentEl = event.target.parentNode;
            if (parentEl) { parentEl.classList.remove('open'); }

            nextEl.classList.add('open');
        } else if (nextEl && nextEl.classList) { nextEl.classList.remove('open'); }
        // return false;
    }

    render() {
        return (
            <React.Fragment>
                <div className="topbar-menu">
                    <div className="container-fluid">
                        <Collapse isOpen={this.props.isMenuOpened} id="navigation">
                            <NavMenuContent onMenuClickCallback={this.onMenuClick} {...this.props} />
                        </Collapse>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default withRouter(connect()(Navbar));
