import { all } from 'redux-saga/effects';
import authSaga from '@Auth/redux/saga';


export default function* rootSaga(getState) {
    yield all([
        authSaga(),
    ]);
}
