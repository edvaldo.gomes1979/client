import { combineReducers } from 'redux';
import Auth from '@Auth/redux/reducers';

export default combineReducers({
    Auth
});