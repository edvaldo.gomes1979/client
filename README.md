# Health Care
Projeto de gerenciamento de Clinicas / Hospitais com objetivo de permitir o gerenciamento integrado de instituições publicas e privadas. Com viés de cuidados clínicos por esquipes multidiciplinar, adminitração e gestão de processos contabéis, financeiros. **TODO: More in README_Health-Care_Project.md**

## Health Care - Client

**Client**: contem a interface de usuário para interações com os servicões / regras definadas (health-care_service)

### IMPORTANT!!
Sempre inicie o projeto de serviços (_health-care_service_) antes do iniciar este cliente (_health-care/client_)
___

### Project Composition / Composição do Projeto
-----
  - React WebApp
  - Client
  - Using _healt-cara_service_

### Main LIBS / Principais LIBS
-----
  - React - (Front Framework)
  - SASS - (CSS)
  - Bootstrap 4.0.0-alpha.6 (Layout Framework)
  - font-awesome - (Icons)
  - axios - (AJAX and API REST client request)

### Requirements 
-----
  - Health Care Service (started)

### Steps - Installation a Development Environment
-----

**1. Install Dependencies**

    yarn install

**2. Start Server**

    yarn start

### Utilities Commands
**Add LIB**

    yarn add lib-name